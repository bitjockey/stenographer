import pytest

from stenographer import transcriber
from stenographer.settings import NPC_AUTHOR
from stenographer.utils import generate_hash

from tests.fixtures import load_file


def test_transcribe_single_character():
    content = "Lauren: [_shakes head_] Nah. I don't want to be an ass to some poor short person behind us. [_grins_] Totally unfair advantage and all that."
    row = dict(
        original_id="12345678",
        author=NPC_AUTHOR,
        nick="god",
        timestamp="2020-02-23 23:58:55.281000",
        content=content,
    )

    assert transcriber.transcribe_row(row) == [
        dict(
            original_id=row["original_id"],
            character="Lauren",
            timestamp=row["timestamp"],
            content="[_shakes head_] Nah. I don't want to be an ass to some poor short person behind us. [_grins_] Totally unfair advantage and all that.",
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash(
                "[_shakes head_] Nah. I don't want to be an ass to some poor short person behind us. [_grins_] Totally unfair advantage and all that."
            ),
        )
    ]


def test_transcribe_multiple_characters():
    content = "AJ: Message One\nRK: Message 2\n AJ: Message 3"
    row = dict(
        original_id="12345678",
        author=NPC_AUTHOR,
        nick="god",
        timestamp="2020-02-23 23:58:55.281000",
        content=content,
    )

    assert transcriber.transcribe_row(row) == [
        dict(
            original_id=row["original_id"],
            character="AJ",
            timestamp=row["timestamp"],
            content="Message One",
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash("Message One"),
        ),
        dict(
            original_id=row["original_id"],
            character="RK",
            timestamp=row["timestamp"],
            content="Message 2",
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash("Message 2"),
        ),
        dict(
            original_id=row["original_id"],
            character="AJ",
            timestamp=row["timestamp"],
            content="Message 3",
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash("Message 3"),
        ),
    ]


def test_transcribe_and_characters():
    content = "Bitjockey and Fastjack: Hello there, friend"
    row = dict(
        original_id="12345678",
        author=NPC_AUTHOR,
        nick="god",
        timestamp="2020-02-23 23:58:55.281000",
        content=content,
    )

    assert transcriber.transcribe_row(row) == [
        dict(
            original_id=row["original_id"],
            character="Bitjockey",
            timestamp=row["timestamp"],
            content="Hello there, friend",
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash("Hello there, friend"),
        ),
        dict(
            original_id=row["original_id"],
            character="Fastjack",
            timestamp=row["timestamp"],
            content="Hello there, friend",
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash("Hello there, friend"),
        ),
    ]


def test_transcribe_inline_colon():
    content = "Bitjockey: I don't know what to be more shocked by: this pandemic or the response to it"
    row = dict(
        original_id="12345678",
        author=NPC_AUTHOR,
        nick="god",
        timestamp="2020-02-23 23:58:55.281000",
        content=content,
    )

    assert transcriber.transcribe_row(row) == [
        dict(
            original_id=row["original_id"],
            character="Bitjockey",
            timestamp=row["timestamp"],
            content="I don't know what to be more shocked by: this pandemic or the response to it",
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash(
                "I don't know what to be more shocked by: this pandemic or the response to it"
            ),
        ),
    ]


@pytest.mark.parametrize(
    "content",
    [
        "[_Amarok moves towards the house. She leads you inside, which is fairly spacious, with a few hallways leading deeper into the house/complex. She takes one hallway and takes you to a room that's set up like a common area: a large low table in the center, a few couches and chairs, plus some electronic equipment. On the low table are plates and some platters of food._]",
        "[_Arcade leads you down a hallway, pointing out the kitchen and a study along the way. He stops a small suite of rooms: two bedrooms joined to a bathroom and a small common area. The bedrooms each have 4 beds._]",
        '(( Annotation in childish handwriting: "this seems dumb" ))',
        "[_The concert continues.\nEventually, though, it has to start wrapping up. The band and Eliza do give in to calls for an encore with good humor. Afterwards, the lights come up and people start to move on to the exits_]",
    ],
)
def test_transcribe_descriptions(content):
    row = dict(
        original_id="12345678",
        author=NPC_AUTHOR,
        nick="god",
        timestamp="2020-02-23 23:58:55.281000",
        content=content,
    )

    assert transcriber.transcribe_row(row) == [
        dict(
            original_id=row["original_id"],
            character="Description",
            timestamp=row["timestamp"],
            content=content,
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash(content),
        ),
    ]


def test_transcribe_complex_markup():
    content = load_file("discord.md")
    row = dict(
        original_id="12345678",
        author=NPC_AUTHOR,
        nick="god",
        timestamp="2020-02-23 23:58:55.281000",
        content=content,
    )

    expected = [
        dict(
            original_id=row["original_id"],
            character="Description",
            timestamp=row["timestamp"],
            content="[_Tiger looks at Kalahan contemplatively_]",
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash("[_Tiger looks at Kalahan contemplatively_]"),
        ),
        dict(
            original_id=row["original_id"],
            character="Tiger",
            timestamp=row["timestamp"],
            content="[_quietly_] ```asciidoc\n= Had only Bull not gotten to you first... =```",
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash(
                "[_quietly_] ```asciidoc\n= Had only Bull not gotten to you first... =```"
            ),
        ),
        dict(
            original_id=row["original_id"],
            character="Tiger",
            timestamp=row["timestamp"],
            content="```asciidoc\n= You may do so. I simply wish her safe in her den. But I cannot and will not force you to do anything. And my power in the physical plane is greatly limited without one to call me mentor. =```",
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash(
                "```asciidoc\n= You may do so. I simply wish her safe in her den. But I cannot and will not force you to do anything. And my power in the physical plane is greatly limited without one to call me mentor. =```"
            ),
        ),
    ]

    assert transcriber.transcribe_row(row) == expected


def test_transcribe_invalid_markdown():
    content = "[_I am missing an underscore]\nTest: _Newline_"
    row = dict(
        original_id="12345678",
        author=NPC_AUTHOR,
        nick="god",
        timestamp="2020-02-23 23:58:55.281000",
        content=content,
    )

    assert transcriber.transcribe_row(row) == [
        dict(
            original_id=row["original_id"],
            character="Description",
            timestamp=row["timestamp"],
            content="[_I am missing an underscore]",
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash("[_I am missing an underscore]"),
        ),
        dict(
            original_id=row["original_id"],
            character="Test",
            timestamp=row["timestamp"],
            content="_Newline_",
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash("_Newline_"),
        ),
    ]


def test_transcribe_unknown_format():
    content = """Copeland:
`Dear Meghan,
I admit I have no idea how to begin this. I have been staring at your message for the past hour, and I am still at a loss. But I think not responding would be worse, so here we are.
I do remember you. I remember the day you came to my door. I've thought about you many times over the years. I wondered if I'd ever be able to give you the apology I owe you. My niece had been the one they'd send, but you were the one there instead. And I knew I couldn't be what you needed then. I didn't know if I could be it for her, but I knew I'd only fail you. I've spent a long time wondering how much.
I don't know how much I'll be able to help you in your search for answers. You were a stranger to me then, and I've been too afraid to look into what became of you after. But I think I owe it to you enough to try, and to offer whatever I can. If you would wish to meet in person, that can be arranged. Otherwise, please don't hesitate to contact me again.
Sincerely,
Jessamyn Copeland`"""
    row = dict(
        original_id="12345678",
        author=NPC_AUTHOR,
        nick="god",
        timestamp="2020-02-23 23:58:55.281000",
        content=content,
    )

    assert transcriber.transcribe_row(row) == [
        dict(
            original_id=row["original_id"],
            character="Description",
            timestamp=row["timestamp"],
            content=content,
            raw_content=row["content"],
            playable=False,
            hash_id=generate_hash(content),
        ),
    ]


def test_transcribe_character_name_with_and():
    character = "Copeland" 
    content = "...yes. Very yes. This...hasn't exactly happened to me before."
    raw_content = f"{character}: {content}"
    row = dict(
        original_id="12345678",
        author=NPC_AUTHOR,
        nick="god",
        timestamp="2020-02-23 23:58:55.281000",
        content=raw_content,
    )

    assert transcriber.transcribe_row(row) == [
        dict(
            original_id=row["original_id"],
            character=character,
            timestamp=row["timestamp"],
            content=content,
            raw_content=raw_content,
            playable=False,
            hash_id=generate_hash(content),
        ),
    ]


def test_transcribe_arrow_symbols():
    character = "Shiro"
    content = "[_signing_]  <This seems odd. By content, date, and location.>"
    expected_content = "[_signing_]  \\<This seems odd. By content, date, and location.\\>"
    raw_content = f"{character}: {content}"
    row = dict(
        original_id="12345678",
        author=NPC_AUTHOR,
        nick="god",
        timestamp="2020-02-23 23:58:55.281000",
        content=raw_content,
    )

    assert transcriber.transcribe_row(row) == [
        dict(
            original_id=row["original_id"],
            character=character,
            timestamp=row["timestamp"],
            content=expected_content,
            raw_content=raw_content,
            playable=False,
            hash_id=generate_hash(expected_content),
        ),
    ]


def test_transcribe_arrow_symbols_inside_description():
    character = "hopper"
    content = "[_looks for <the meaning of life>_]"
    expected_content = "[_looks for \\<the meaning of life\\>_]"
    raw_content = f"{character}: {content}"
    row = dict(
        original_id="12345678",
        author=NPC_AUTHOR,
        nick="god",
        timestamp="2020-02-23 23:58:55.281000",
        content=raw_content,
    )

    assert transcriber.transcribe_row(row) == [
        dict(
            original_id=row["original_id"],
            character=character,
            timestamp=row["timestamp"],
            content=expected_content,
            raw_content=raw_content,
            playable=False,
            hash_id=generate_hash(expected_content),
        ),
    ]
