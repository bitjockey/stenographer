"""Process raw chat logs into one for import into Diskhorde"""
import os
import csv
import pathlib

from stenographer.settings import NPC_AUTHOR
from stenographer.transcriber import transcribe_row
from stenographer.utils import read_csv, progress, generate_hash, sanitize_author
from stenographer.exceptions import MissingRequiredSetting


def process_csv(input_filename, output_filename):
    if NPC_AUTHOR is None:
        raise MissingRequiredSetting("NPC_AUTHOR")

    output_file_exists = pathlib.Path(output_filename).exists()
    input_rows = read_csv(input_filename)
    total = len(input_rows)
    count = 0

    with open(output_filename, "w+") as csv_file:
        fieldnames = [
            "original_id",
            "character",
            "timestamp",
            "content",
            "raw_content",
            "playable",
            "hash_id",
        ]
        writer = csv.DictWriter(
            csv_file,
            fieldnames=fieldnames,
            quoting=csv.QUOTE_ALL,
            lineterminator=os.linesep,
        )

        if not output_file_exists:
            writer.writeheader()

        for row in input_rows:
            if row["author"] == NPC_AUTHOR:
                processed_rows = transcribe_row(row)
                for processed_row in processed_rows:
                    writer.writerow(processed_row)
            else:
                writer.writerow(
                    dict(
                        original_id=row["original_id"],
                        character=row["nick"]
                        if row["nick"]
                        else sanitize_author(row["author"]),
                        timestamp=row["timestamp"],
                        content=row["content"],
                        raw_content=row["content"],
                        playable=True,
                        hash_id=generate_hash(row["content"]),
                    )
                )

            count += 1
            progress(count, total, status="")
