import json
import csv
import configparser
import datetime as dt
import pytz
import pathlib
import re
import sys
import hashlib


utc = pytz.UTC


def get_config(filename="config.ini"):
    config = configparser.ConfigParser()
    config.read(filename)
    return config


def get_datetime_string(datetime, fmt="%Y-%m-%d-%H%M%S"):
    return datetime.strftime(fmt)


def get_current_datetime():
    return normalize_datetime(dt.datetime.now())


def get_datetime(datestring, fmt="%Y-%m-%d"):
    return normalize_datetime(dt.datetime.strptime(datestring, fmt))


def normalize_datetime(datetime):
    return datetime.replace(tzinfo=utc)


def read_csv(filename):
    with open(filename, newline="") as csv_file:
        reader = csv.DictReader(csv_file)
        return [row for row in reader]


def read_history(filename):
    """Only get the last few"""
    return read_csv(filename)


def read_json(filename):
    """Read json file"""
    with open(filename, "r") as f:
        return json.load(f)


def write_history(csv_filename, filename, from_date, to_date, request_date):
    history_file_exists = pathlib.Path(csv_filename).exists()
    with open(csv_filename, "a") as csv_file:
        fieldnames = ["request_date", "from_date", "to_date", "filename"]
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        if not history_file_exists:
            writer.writeheader()
        writer.writerow(
            dict(
                request_date=get_datetime_string(request_date),
                from_date=get_datetime_string(from_date),
                to_date=get_datetime_string(to_date),
                filename=filename,
            )
        )


def progress(count, total, status=""):
    # From https://gist.github.com/vladignatyev/06860ec2040cb497f0f3
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = "=" * filled_len + "-" * (bar_len - filled_len)

    sys.stdout.write("[%s] %s%s ...%s\r" % (bar, percents, "%", status))
    sys.stdout.flush()


def generate_hash(text):
    h = hashlib.sha256(text.encode())
    return h.hexdigest()


def sanitize_author(author):
    p = re.compile(r"(\w+)")
    return p.match(author).group()
