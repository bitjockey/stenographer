import click

from pathlib import Path

from stenographer.bot import start_bot
from stenographer.processor import process_csv
from stenographer.server import start_server
from stenographer.settings import CLIENT_ID, HOST, PORT, DEBUG
from stenographer.utils import read_csv


@click.group()
def cli():
    pass


@cli.command()
@click.option("--input-filename", "-f", required=True)
@click.option("--output-filename", "-o", required=True)
def process(input_filename, output_filename):
    """Process the raw chat log"""
    process_csv(input_filename, output_filename)


@cli.command()
@click.option("--input-dir", "-i", required=True)
@click.option("--output-dir", "-o", required=True)
def batch_process(input_dir, output_dir):
    """Process the raw chat logs in the input dir"""
    input_dir_path = Path(input_dir)
    output_dir_path = Path(output_dir)
    history_file_path = input_dir_path.joinpath("export_history.csv")

    if not history_file_path.exists():
        print(f"{str(history_file_path)} does not exist.")
        return

    if not output_dir_path.exists():
        output_dir_path.mkdir(parents=True)

    history = read_csv(str(history_file_path))

    for row in history:
        input_file_path = input_dir_path.joinpath(row["filename"])
        output_file_path = output_dir_path.joinpath(row["filename"])
        process_csv(str(input_file_path), str(output_file_path))


@cli.command()
@click.option("--host", "-h", required=True, default=HOST)
@click.option("--port", "-p", required=True, default=PORT)
@click.option("--debug/--no-debug", default=DEBUG)
def server(host, port, debug):
    """Start the server"""
    start_server(host=host, port=port, debug=debug)


@cli.command()
def start():
    """Start the bot"""
    show_invite()
    start_bot()


@cli.command()
def invite():
    """Show invite link"""
    show_invite()


def show_invite():
    """Generate a server invite link"""
    if CLIENT_ID is None:
        raise ValueError("Please set a DISCORD_CLIENT_ID in your env vars")

    url = f"https://discordapp.com/oauth2/authorize?client_id={CLIENT_ID}&scope=bot"
    print(f"Visit this link to invite the bot to your server: {url}")
