from discord.ext import tasks, commands

from stenographer.settings import (
    CHANNEL_ID,
    EXPORT_SCHEDULE,
    DEFAULT_START_DATETIME,
    DATA_DIR,
)
from stenographer.models import (
    DiscordMessage,
    CharacterMessage,
    Player,
)
from stenographer.utils import (
    get_datetime,
    get_datetime_string,
    read_json,
    normalize_datetime,
)


class Importer(commands.Cog):
    def __init__(self, bot):
        self.index = 0
        self.bot = bot
        self.channel = None
        self.from_date = None
        self.from_datestring = None
        self.to_date = None
        self.to_datestring = None
        self.importer.start()

    def cog_unload(self):
        self.importer.cancel()

    def update_users(self):
        print("Updating users")

        users_filepath = DATA_DIR.joinpath("users.json")

        if not users_filepath.exists():
            print(f"{users_filepath} does not exist, skipping update")
            return

        users = read_json(users_filepath)

        for user in users["players"]:
            if "role" not in user:
                user["role"] = Player.DEFAULT_ROLE
            Player.upsert(user, identifier_keys=["author_id"])

    def update_message(self, message):
        discord_message = DiscordMessage(message)
        self.update_discord_message(discord_message)
        self.update_character_messages(discord_message)

    def update_discord_message(self, discord_message):
        DiscordMessage.upsert(discord_message)

    def update_character_messages(self, discord_message):
        CharacterMessage.upsert(
            discord_message.to_character_messages(), identifier_keys=["hash_id"]
        )

    def get_from_date(self):
        from_datestring = DEFAULT_START_DATETIME
        from_date = get_datetime(DEFAULT_START_DATETIME, fmt="%Y-%m-%d-%H%M%S")

        # Check if we should actually resume
        last_character_message = CharacterMessage.last()
        if last_character_message:
            from_date = normalize_datetime(last_character_message["timestamp"])
            from_datestring = get_datetime_string(from_date)
            print(f"Resuming from {from_datestring}")

        self.from_date = from_date
        self.from_datestring = from_datestring

    async def get_to_date(self):
        last_message = await self.channel.fetch_message(self.channel.last_message_id)
        to_date = normalize_datetime(last_message.created_at)
        to_datestring = get_datetime_string(to_date)
        self.to_date = to_date
        self.to_datestring = to_datestring

    @tasks.loop(**EXPORT_SCHEDULE)
    async def importer(self):
        """Background importer"""
        print("Starting background importer")

        self.update_users()

        if not self.channel:
            self.channel = self.bot.get_channel(int(CHANNEL_ID))

        # Resume or start from this point
        self.get_from_date()
        # Stop until this point
        await self.get_to_date()

        print(f"Starting from {self.from_datestring} to {self.to_datestring}")

        if self.from_datestring == self.to_datestring:
            print("Skipping, because there are no new messages")
            return

        while self.from_date <= self.to_date:
            async for message in self.channel.history(
                after=self.from_date, limit=100, oldest_first=True
            ):
                self.update_message(message)
                self.from_date = message.created_at

            if self.from_date == self.to_date:
                break

        status = f"Successfully imported {self.channel.name} from {self.from_datestring} to {self.to_datestring}"
        print(status)

    @importer.before_loop
    async def before_importer(self):
        print("waiting to load...")
        await self.bot.wait_until_ready()
