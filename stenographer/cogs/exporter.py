import csv

from pathlib import Path

import discord
from discord.ext import tasks, commands

from stenographer.settings import (
    CHANNEL_ID,
    EXPORT_CHANNEL_ID,
    EXPORT_DIR,
    EXPORT_SCHEDULE,
    PROCESSED_DIR,
    DEFAULT_START_DATETIME,
)
from stenographer.models import DiscordMessage, CharacterMessage
from stenographer.processor import process_csv
from stenographer.storage import storage
from stenographer.utils import (
    get_datetime,
    get_current_datetime,
    get_datetime_string,
    write_history,
    read_history,
)


class Exporter(commands.Cog):
    def __init__(self, bot):
        self.index = 0
        self.bot = bot
        self.exporter.start()

    def cog_unload(self):
        self.exporter.cancel()

    def process_existing(self, history):
        print("Process existing exports")
        processed_dir_path = Path(PROCESSED_DIR)
        processed_exports = processed_dir_path.glob("**/*.csv")
        for export in history:
            if export["filename"] not in processed_exports:
                export_file_path = Path(EXPORT_DIR, export["filename"])
                processed_file_path = Path(PROCESSED_DIR, export["filename"])
                process_csv(str(export_file_path), str(processed_file_path))
                print(f"Processed {str(processed_file_path)}")

    @tasks.loop(**EXPORT_SCHEDULE)
    async def exporter(self):
        """Background exporter"""
        print("Starting background exporter")
        request_date = get_current_datetime()

        channel = self.bot.get_channel(int(CHANNEL_ID))
        export_channel = self.bot.get_channel(int(EXPORT_CHANNEL_ID))
        print("Does this work? channel", channel)

        history_file_path = Path(EXPORT_DIR, "export_history.csv")

        if history_file_path.exists():
            print("Existing history file")
            history = read_history(str(history_file_path))
            self.process_existing(history)
            from_datestring = history[-1]["to_date"]
        else:
            print("No history file found")
            from_datestring = DEFAULT_START_DATETIME

        from_date = get_datetime(from_datestring, fmt="%Y-%m-%d-%H%M%S")
        last_message = await channel.fetch_message(channel.last_message_id)
        to_date = last_message.created_at
        to_datestring = get_datetime_string(to_date)

        if from_datestring == to_datestring:
            print("Skipping, because there are no new messages")
            return

        filename = f"{get_datetime_string(from_date)}_{get_datetime_string(to_date)}.{get_current_datetime().timestamp()}.csv"
        file_path = Path(EXPORT_DIR, filename)
        processed_file_path = Path(PROCESSED_DIR, filename)

        with open(str(file_path), "w+", newline="") as csv_file:
            fieldnames = DiscordMessage.FIELDNAMES

            writer = csv.DictWriter(
                csv_file, fieldnames=fieldnames, quoting=csv.QUOTE_ALL
            )
            writer.writeheader()

            while from_date <= to_date:
                async for message in channel.history(
                    after=from_date, limit=100, oldest_first=True
                ):
                    discord_message = DiscordMessage(message)
                    writer.writerow(discord_message.to_dict())
                    storage.upsert(
                        DiscordMessage.COLLECTION_NAME, discord_message.to_dict(),
                    )
                    storage.upsert(
                        CharacterMessage.COLLECTION_NAME,
                        discord_message.to_character_messages(),
                    )
                    from_date = message.created_at

                if from_date == to_date:
                    break

        status = f"Successfully exported logs for {channel.name} from {from_datestring} to {get_datetime_string(to_date)}"
        print(status)
        write_history(
            str(history_file_path), filename, from_date, to_date, request_date
        )

        # Filenames to display in the message
        export_filename = f"{from_datestring}__{to_datestring}.csv"
        processed_filename = f"processed.{export_filename}"

        process_csv(str(file_path), str(processed_file_path))

        await export_channel.send(
            f"Exported logs from {from_datestring} to {to_datestring}",
            files=[
                discord.File(str(file_path), filename=export_filename),
                discord.File(str(processed_file_path), filename=processed_filename),
            ],
        )

    @exporter.before_loop
    async def before_exporter(self):
        print("waiting to load...")
        await self.bot.wait_until_ready()
