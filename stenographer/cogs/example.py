from pathlib import Path
from discord.ext import tasks, commands

from stenographer.settings import CHANNEL_ID
from stenographer.utils import (
    write_history,
    read_history,
    get_datetime,
    get_datetime_string,
    get_current_datetime,
)


class Example(commands.Cog):
    def __init__(self, bot):
        self.index = 0
        self.bot = bot
        self.printer.start()

    @tasks.loop(seconds=10)
    async def printer(self):
        print(self.index)
        print("Debug")
        request_date = get_current_datetime()
        history_file_path = Path("history.csv")
        channel = self.bot.get_channel(int(CHANNEL_ID))

        if history_file_path.exists():
            print("Existing history file")
            history = read_history(str(history_file_path))
            from_date = get_datetime(history[-1]["to_date"], fmt="%Y-%m-%d-%H%M%S")
        else:
            print("No history file found")
            from_date = get_datetime("2020-01-01")

        print("Getting last message")

        last_message = await channel.fetch_message(channel.last_message_id)
        to_date = last_message.created_at

        if get_datetime_string(from_date) == get_datetime_string(to_date):
            print("Skipping")
            return

        print("from_date:", from_date, "to_date:", to_date)

        write_history(
            str(history_file_path), "fuckthis.csv", from_date, to_date, request_date
        )

    @printer.before_loop
    async def before_printer(self):
        print("Waiting")
        await self.bot.wait_until_ready()
