from bottle import route, run, request, post

from stenographer.decorators import require_api_key


@post("/hello")
@require_api_key
def hello():
    name = request.json.get("name")
    return dict(name=name)


def start_server(host, port, debug):
    return run(host=host, port=port, debug=debug, reloader=debug)
