import json
from typing import Dict, Union, List

import discord

from stenographer.storage import storage
from stenographer.transcriber import transcribe_row
from stenographer.utils import generate_hash


IGNORE_PREFIX = "_"
DUNGEON_MASTER = "dungeon_master"
PLAYER = "player"


class BaseDocument:
    def __init__(
        self, data, base_fields: List[str], add_fields: List[str] = None
    ) -> None:
        self._data = self.sanitize(data)
        self._fields = base_fields + (add_fields or [])

    def sanitize(self, data):
        if isinstance(data, Dict):
            # Typecast to string so GraphQL can support it
            for k, v in data.items():
                if "_id" in k and isinstance(v, int):
                    data[k] = str(v)
        else:
            for k in dir(data):
                if "_id" in k:
                    v = getattr(data, k)
                    setattr(data, k, str(v))
        return data

    @classmethod
    def find(cls, *args, **kwargs):
        """Returns a list of results"""
        return list(map(cls, cls.find_cursor(*args, **kwargs)))

    @classmethod
    def find_cursor(cls, *args, **kwargs):
        """Returns a cursor"""
        return storage.db[cls.COLLECTION_NAME].find(*args, **kwargs)

    @classmethod
    def find_one(cls, *args, **kwargs):
        """Returns an instance of the class"""
        return cls(storage.db[cls.COLLECTION_NAME].find_one(*args, **kwargs))

    @classmethod
    def upsert(cls, obj, identifier_keys: List[str] = None):
        def convert(o):
            if not isinstance(o, cls):
                o = cls(o)
            return o.to_dict()

        if isinstance(obj, List):
            obj = list(map(convert, obj))
        else:
            obj = convert(obj)

        storage.upsert(cls.COLLECTION_NAME, obj, identifier_keys=identifier_keys)

    @classmethod
    def last(cls):
        """Return the last object"""
        results = storage.query(
            cls.COLLECTION_NAME, sort="$natural", order="DESC", limit=1
        )
        return results[0] if results else None

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, updatedData):
        self._data = updatedData

    @property
    def fields(self):
        return self._fields

    def from_dict(self, data: Dict):
        if not isinstance(data, Dict):
            return

        for k, v in data.items():
            if k in self.fields:
                setattr(self, k, v)

    def to_json(self):
        return json.dumps(self.__dict__)

    def to_dict(self):
        return dict(
            list(
                filter(
                    lambda i: not i[0].startswith(IGNORE_PREFIX), self.__dict__.items()
                )
            )
        )


class User(BaseDocument):
    COLLECTION_NAME = "users"
    FIELDNAMES = ["author_id"]

    def __init__(self, data, add_fields: List[str] = None):
        super().__init__(data=data, base_fields=self.FIELDNAMES, add_fields=add_fields)
        self.from_dict(self.data)


class Player(User):
    DEFAULT_ROLE = "player"
    ADD_FIELDS = ["characters", "role", "default_character"]

    def __init__(self, data, role: str = PLAYER):
        self.role = role
        self.default_character = (
            data.get("characters")[0] if (data and data.get("characters")) else None
        )
        super().__init__(data=data, add_fields=self.ADD_FIELDS)

    def is_dm(self):
        return self.role == DUNGEON_MASTER


class DMPlayer(Player):
    def __init__(self, data):
        super().__init__(data, DUNGEON_MASTER)


class DiscordMessage(BaseDocument):
    COLLECTION_NAME = "messages"
    FIELDNAMES = [
        "original_id",
        "author_id",
        "author",
        "nick",
        "timestamp",
        "edited_timestamp",
        "content",
        "channel_id",
    ]

    def __init__(
        self, message: Union[discord.Message, Dict], add_fields: List[str] = None
    ):
        super().__init__(
            data=message, base_fields=self.FIELDNAMES, add_fields=add_fields
        )
        if isinstance(message, Dict):
            self.from_dict(self.data)
        elif isinstance(message, discord.Message):
            self.from_message(self.data)
            self.data = self.sanitize(self.data)

    def from_message(self, message: discord.Message):
        self.original_id = message.id
        self.author_id = message.author.id
        self.author = message.author.name
        self.nick = self._get_nick(message)
        self.timestamp = message.created_at
        self.edited_timestamp = message.edited_at
        self.content = message.clean_content
        self.channel_id = message.channel.id

    def _get_nick(self, message: discord.Message):
        return message.author.nick or message.author.display_name

    def to_character_messages(self):
        rows = transcribe_row(self.to_dict())
        return [CharacterMessage(row).to_dict() for row in rows]


class CharacterMessage(DiscordMessage):
    COLLECTION_NAME = "character_messages"
    ADD_FIELDS = [
        "character",
        "content",
        "raw_content",
        "hash_id",
    ]

    def __init__(self, obj: Union[Dict, discord.Message]):
        super().__init__(obj, add_fields=self.ADD_FIELDS)

    def from_dict(self, obj: Dict):
        if "character" in obj:
            if bool(obj["character"]):
                self.character = obj["character"]
        else:
            self.character = self._get_character(obj["author_id"])

        self.content = obj["content"]
        self.raw_content = obj["raw_content"]
        self.hash_id = generate_hash(self.content)
        super().from_dict(obj)

    def from_message(self, obj: discord.Message):
        self.character = self._get_character(obj.author.id)
        self.content = self.data.content
        self.raw_content = self.data.content
        self.hash_id = generate_hash(self.content)
        super().from_message(obj)

    def _get_character(self, author_id: int):
        player = Player.find_one({"author_id": author_id})
        return player.default_character
