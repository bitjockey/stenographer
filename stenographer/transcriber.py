"""Converts the row content into a dialogue"""
import re

from copy import copy
from collections import Counter
from typing import Dict, List
from discord_markdown.lexer import tokenize
from discord_markdown.spec import TERMINAL_TOKEN_TYPES, FORMAT_TOKEN_TYPES

from stenographer.utils import generate_hash
from stenographer.settings import NPC_AUTHOR_ID

DESCRIPTION = "Description"
TRANSCRIPT_PATTERN = re.compile(
    r"(\[_[\s\S]+?\]|\(\(.*?\)\))|^(\w+|\w+\sand\s\w+):([\s\S]+)"
)
CUSTOM_ENCLOSURE = re.compile(r"<(.*?)>")


def transcribe_row(row: Dict) -> List[Dict]:
    row = copy(row)
    rows = []
    raw_content = row["content"]
    paragraphs = split_paragraphs(raw_content)
    # print(paragraphs)

    for paragraph in paragraphs:
        if CUSTOM_ENCLOSURE.search(paragraph.strip()):
            paragraph = _handle_arrow_brackets(paragraph)

        parts = TRANSCRIPT_PATTERN.findall(paragraph.strip())
        # print(parts)
        if not parts:
            rows.append(transcribe_default(row))
            break
            # print(row)

        for description, character_string, dialogue in parts:
            characters = []

            if description and row["author_id"] == NPC_AUTHOR_ID:
                dialogue = description
                characters.append(DESCRIPTION)
            elif description and row["author_id"] != NPC_AUTHOR_ID:
                row = transcribe_default(row)
                rows.append(row)
            elif " and " in character_string:
                characters = characters + [
                    character.strip() for character in character_string.split("and")
                ]
            else:
                characters.append(character_string.strip())

            for character in characters:
                row.update(
                    {
                        "character": character,
                        "content": dialogue.strip(),
                        "raw_content": raw_content,
                        "hash_id": generate_hash(dialogue.strip()),
                    }
                )
                rows.append(row)

    return rows


def transcribe_default(row):
    row.update(
        {"raw_content": row["content"], "hash_id": generate_hash(row["content"]),}
    )
    return row


def split_paragraphs(text: str) -> List[str]:
    tokens = tokenize(text)
    tokens_iter = iter(tokens)
    eof = tokens[-1]
    current_token = next(tokens_iter)
    format_tokens = []
    paragraph = []
    paragraph_count = 0
    paragraphs = []
    format_counts = get_format_counts(tokens)

    while current_token != eof:

        if current_token.type in FORMAT_TOKEN_TYPES:

            format_counts[current_token.type] -= 1

            if format_tokens and current_token.type == format_tokens[-1].type:
                format_tokens.pop()
            else:
                format_tokens.append(current_token)

            # print(f"[{current_token.type}]")

        paragraph.append(current_token.value)
        current_token = next(tokens_iter)

        if should_split(current_token, format_tokens, tokens):
            # print("-------------------------------------")
            paragraph_count += 1
            paragraphs.append("".join(paragraph))
            paragraph = []

    # print(paragraph_count)
    return paragraphs


def should_split(current_token, format_tokens, tokens):
    if current_token.type not in TERMINAL_TOKEN_TYPES:
        return False

    if validate_syntax(tokens):
        return len(format_tokens) == 0
    else:
        if format_tokens:
            format_tokens.pop()

    return True


def validate_syntax(tokens):
    format_counts = get_format_counts(tokens)
    return all(i % 2 == 0 for i in format_counts.values())


def get_format_counts(tokens):
    token_types = [token.type for token in tokens]

    def is_format_type(token_type):
        return token_type in FORMAT_TOKEN_TYPES

    format_tokens = list(filter(is_format_type, token_types))
    return Counter(format_tokens)


def get_invalid(tokens):
    format_counts = get_format_counts(tokens)
    invalid = list(filter(lambda kv: kv[1] % 2 != 0, list(format_counts.items())))
    return dict(invalid)


def _handle_arrow_brackets(string):
    return string.replace("<", "\\<").replace(">", "\\>")
