class MissingRequiredSetting(Exception):
    def __init__(self, setting_name):
        self.setting_name = setting_name

    def __str__(self):
        return repr(f"{self.setting_name} must be set")
