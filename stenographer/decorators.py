from functools import wraps
from bottle import request, abort

from stenographer.settings import API_KEY


def require_api_key(view_function):
    # https://coderwall.com/p/4qickw/require-an-api-key-for-a-route-in-flask-using-only-a-decorator
    @wraps(view_function)
    def validate_api_key(*args, **kwargs):
        if request.headers.get("apikey") and request.headers.get("apikey") == API_KEY:
            return view_function(*args, **kwargs)
        else:
            abort(401)

    return validate_api_key
