import csv
import logging
import os
from pathlib import Path

import asyncio
import discord
from discord.ext import commands

from stenographer.settings import (
    LOG_DIR,
    EXPORT_DIR,
    PROCESSED_DIR,
    BOT_TOKEN,
    CHANNEL_ID,
    COMMAND_PREFIX,
)
from stenographer.cogs import Exporter, Importer
from stenographer.models import DiscordMessage
from stenographer.storage import storage
from stenographer.utils import get_current_datetime, get_datetime, get_datetime_string

DESCRIPTION = "Hello, I am the stenographer. I can export messages from the channel you configured."

log_dir_path = Path(LOG_DIR)
if not log_dir_path.exists():
    log_dir_path.mkdir(parents=True)

export_dir_path = Path(EXPORT_DIR)
if not export_dir_path.exists():
    export_dir_path.mkdir(parents=True)

processed_dir_path = Path(PROCESSED_DIR)
if not processed_dir_path.exists():
    processed_dir_path.mkdir(parents=True)

now = get_current_datetime()
logger = logging.getLogger("stenographer")
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(
    filename=os.path.join(LOG_DIR, f"{get_datetime_string(now)}-discord.log"),
    encoding="utf-8",
    mode="w",
)
handler.setFormatter(
    logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s")
)
logger.addHandler(handler)

intents = discord.Intents.default()
intents.members = True
intents.message_content = True
intents.dm_messages = True
bot = commands.Bot(
    command_prefix=COMMAND_PREFIX, description=DESCRIPTION, intents=intents
)


@bot.event
async def on_ready():
    print(DESCRIPTION, CHANNEL_ID)


@bot.command()
async def export(ctx, from_datestring, to_datestring=None):
    """
    Usage:

        You can send me a DM with a message like below, where `YYYY-MM-dd` is the date format:

            !export 2018-06-01

        Once I receive your request for export, I'll try to get that going, and then I will send you a CSV of the chat log.
    """

    channel = bot.get_channel(int(CHANNEL_ID))

    from_date = get_datetime(from_datestring)

    if not to_datestring:
        # Get latest message timesteamp
        last_message = await channel.fetch_message(channel.last_message_id)
        to_date = last_message.created_at
    else:
        to_date = get_datetime(to_datestring)

    filename = f"{get_datetime_string(from_date)}_{get_datetime_string(to_date)}.{get_current_datetime().timestamp()}.csv"
    status = f"Export request sent by {ctx.message.author} in {ctx.message.channel}. Will write to {filename}"
    print(status)
    logger.info(status)

    await ctx.channel.send(status)

    with open(os.path.join(EXPORT_DIR, filename), "w+", newline="") as csvfile:
        fieldnames = DiscordMessage.FIELDNAMES
        writer = csv.DictWriter(
            csvfile,
            fieldnames=fieldnames,
            quoting=csv.QUOTE_ALL,
            lineterminator=os.linesep,
        )
        writer.writeheader()

        while from_date <= to_date:
            logger.debug(f"Logging from {from_date} {to_date}")

            async for message in channel.history(
                after=from_date, limit=100, oldest_first=True
            ):
                if not message.author.bot:
                    discord_message = DiscordMessage(message)
                    storage.upsert(
                        DiscordMessage.COLLECTION_NAME, discord_message.to_dict(),
                    )
                    writer.writerow(discord_message.to_dict)

                from_date = message.created_at

            if from_date == to_date:
                break

        print(f"Exported to {filename}")

    status = f"Successfully exported logs for {channel.name} from {from_datestring} to {get_datetime_string(to_date)}"
    logger.info(status)
    await ctx.channel.send(
        "Exported", file=discord.File(os.path.join(EXPORT_DIR, filename))
    )


async def main():
    async with bot:
        await bot.add_cog(Importer(bot))
        await bot.start(BOT_TOKEN)


def start_bot():
    logger.info("Starting stenographer")
    asyncio.run(main())
