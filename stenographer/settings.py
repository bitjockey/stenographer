import os
from pathlib import Path

from dotenv import load_dotenv

load_dotenv()

# Bot
CLIENT_ID = os.getenv("DISCORD_CLIENT_ID")
BOT_TOKEN = os.getenv("DISCORD_BOT_TOKEN")
CHANNEL_ID = os.getenv("DISCORD_CHANNEL_ID")
EXPORT_CHANNEL_ID = os.getenv("DISCORD_EXPORT_CHANNEL_ID")
EXPORT_DIR = os.getenv("EXPORT_DIR", os.path.join("chats", CHANNEL_ID))
PROCESSED_DIR = os.getenv(
    "PROCESSED_DIR", os.path.join("chats", CHANNEL_ID, "processed")
)
LOG_DIR = os.getenv("LOG_DIR", "logs")
COMMAND_PREFIX = os.getenv("COMMAND_PREFIX", "!")

# Background Task
DEFAULT_START_DATETIME = os.getenv("DEFAULT_START_DATETIME", "2020-01-01-000000")
SCHEDULE_DURATION = os.getenv("SCHEDULE_DURATION", 1)
SCHEDULE_TYPE = os.getenv("SCHEDULE_TYPE", "minutes")
EXPORT_SCHEDULE = {SCHEDULE_TYPE: int(SCHEDULE_DURATION)}

# Server
HOST = os.getenv("HOST", "127.0.0.1")
PORT = os.getenv("PORT", 8080)
DEBUG = os.getenv("DEBUG", True)
API_KEY = os.getenv("API_KEY", "LOCAL_DEV")

# Transcriber
NPC_AUTHOR = os.getenv("NPC_AUTHOR", None)
NPC_AUTHOR_ID = os.getenv("NPC_AUTHOR_ID", None)

# MongoDB
MONGO_DB_NAME = os.getenv("MONGO_DB_NAME", "chat_history")
MONGO_ROOT_USERNAME = os.getenv("MONGO_ROOT_USERNAME")
MONGO_ROOT_PASSWORD = os.getenv("MONGO_ROOT_PASSWORD")
MONGO_HOST = os.getenv("MONGO_HOST")
MONGO_CONNECTION_STRING = f"mongodb://{MONGO_ROOT_USERNAME}:{MONGO_ROOT_PASSWORD}@{MONGO_HOST}/{MONGO_DB_NAME}?authSource=admin"
MONGO_URL = os.getenv("MONGO_URL", MONGO_CONNECTION_STRING)

# Data and Configuration Files
ROOT_DIR = Path(__file__).parent.parent

DEFAULT_CONF_DIR = ROOT_DIR.joinpath("conf")
CONF_DIR = Path(os.getenv("CONF_DIR", DEFAULT_CONF_DIR)).resolve()

DEFAULT_DATA_DIR = ROOT_DIR.joinpath("data")
DATA_DIR = Path(os.getenv("DATA_DIR", DEFAULT_DATA_DIR)).resolve()
