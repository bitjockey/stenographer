# Stenographer

![stenographer icon](img/128x128.png)

This is a bot to export chat logs from a particular date to the current time. It also exports.

- [Stenographer](#stenographer)
  - [TODO](#todo)
  - [Commands](#commands)
  - [Pre-configuration](#pre-configuration)
  - [Virtualenv](#virtualenv)
  - [Docker](#docker)
  - [MongoDB](#mongodb)
  - [Deploy to dokku](#deploy-to-dokku)
  - [Server](#server)
  - [Credits](#credits)

## TODO

- [ ] Use [background tasks](https://github.com/Rapptz/discord.py/blob/async/examples/background_task.py) to watch for changes in the configured channel
- [ ] Use the background task above to publish updates to the Matrix Chat Log viewer thing

## Commands

`!export YYYY-mm-dd` will export logs from the date specified in `YYYY-mm-dd` format. ex. `!export 2018-10-01` will fetch logs from October 1, 2018 to the current date.

If you've customized the `COMMAND_PREFIX` and changed it to, say, `?`, then type in `?export YYYY-mm-dd`.

## Pre-configuration

This needs to be done BEFORE setting up `stenographer`.

Create a `.env` file within this directory containing these settings:

```bash
DISCORD_CLIENT_ID=<client id of the bot app on discord dev>
DISCORD_BOT_TOKEN=<bot token - get this on the discord dev portal>
DISCORD_CHANNEL_ID=<id for channel you want to extract chat logs from>
DISCORD_EXPORT_CHANNEL_ID=<id for channel you want the exported logs to be uploaded to>
NPC_AUTHOR=<discord username of user acting as NPC (so your GM)>
COMMAND_PREFIX=<optional - set a custom command prefix. Default is !>
API_KEY=<an API key for the server>
```

## Virtualenv

Requirements:

- Python 3.6
- [poetry](https://python-poetry.org/)

Use `poetry` to install requirements along with an executable.

```bash
poetry install
```

Then, to start the bot:

```bash
poetry shell
stenographer start
```

Or do this:

```
poetry run stenographer start
```

## Docker

```bash
docker build -t stenographer .
docker run -it -v $(pwd)/chats:/app/chats --rm --name stenographer stenographer
```

## MongoDB

This stores chat logs in `mongodb`:

```shell
docker run --name mongodb -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=supersecret -e MONGO_INITDB_DATABASE=stenographer -v data:/data/db mongo
```

## Deploy to dokku

On the server:

```
dokku apps:create stenographer
dokku config:set stenographer DISCORD_CLIENT_ID=<DISCORD CLIENT ID>
dokku config:set stenographer DISCORD_BOT_TOKEN=<DISCORD BOT TOKEN>
dokku config:set stenographer DISCORD_CHANNEL_ID=<DISCORD CHANNEL ID>
dokku config:set stenographer DISCORD_EXPORT_CHANNEL_ID=<DISCORD CHANNEL ID TO UPLOAD THE TRANSCRIPTS>
dokku config:set stenographer NPC_AUTHOR=<DISCORD USERNAME>

# This mounts the diskhorde chat logs as the chats folder
dokku storage:mount stenographer /var/lib/dokku/data/storage/diskhorde/chats:/app/chats
```

Then, on your local machine:

```
# Set the dokku git remote url
git remote add dokku dokku@bitjockey.dev:stenographer

# Then deploy
git push dokku master
```

## Server

Testing the server, after running `stenographer server`:

```bash
curl -X POST -H "apikey: LOCAL_DEV" -H "Content-Type: application/json" -d '{"name": "AJ"}' http://127.0.0.1:8080/hello
```

## Credits

<div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/"                 title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"                 title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>